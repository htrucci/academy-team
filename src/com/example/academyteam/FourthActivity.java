package com.example.academyteam;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class FourthActivity extends Activity implements OnClickListener {
	Button btn1;
	Button btn2;
	Button btn3;
	Button btn4;
	Button btn5;
	Button btn7;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.fourth_activity);
		btn1 = (Button) findViewById(R.id.btn1);// btn1 계정관리
		btn2 = (Button) findViewById(R.id.btn2);// btn2 앱정보
		btn3 = (Button) findViewById(R.id.btn3);// btn3 버전정보
		btn4 = (Button) findViewById(R.id.btn4);
		btn5=(Button)findViewById(R.id.btn5);
		
		btn7=(Button)findViewById(R.id.btn7);
		
		btn1.setOnClickListener(this);
		btn2.setOnClickListener(this);
		btn3.setOnClickListener(this);
		btn4.setOnClickListener(this);
		btn5.setOnClickListener(this);
		btn7.setOnClickListener(this);

		

	
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.getId() == btn1.getId()) {
			Intent intent = new Intent(this, AccountManagement2.class);
			startActivity(intent);
		}
		if (v.getId() == R.id.btn2) {
			Builder builder = new AlertDialog.Builder(FourthActivity.this);
			builder.setTitle("앱정보");
			builder.setMessage("호서대학교 황교빈,김태남,배운식,이우현이 제작한 가게부 애플리케이션입니다.");
			builder.setCancelable(false);
			builder.setPositiveButton("확인",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							return;
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
		}

		if (v.getId() == R.id.btn3) {
			Builder builder = new AlertDialog.Builder(FourthActivity.this);
			builder.setTitle("버전정보");
			builder.setMessage("버전 2.0.4.3");
			builder.setCancelable(false);
			builder.setPositiveButton("확인",
					new DialogInterface.OnClickListener() {

						@Override
						public void onClick(DialogInterface dialog, int which) {
							// TODO Auto-generated method stub
							return;
						}
					});
			AlertDialog dialog = builder.create();
			dialog.show();
		}
		 
		if(v.getId()==R.id.btn4){
			Intent intent=new Intent(this,Notice.class);
			startActivity(intent);
		}
		if(v.getId()==R.id.btn5){
			Intent intent=new Intent(this,UseInformation.class);
			startActivity(intent);
		}
		if(v.getId()==R.id.btn7){
			Intent intent = new Intent(Intent.ACTION_SEND);
			intent.setType("message/rfc822");
			intent.putExtra(Intent.EXTRA_EMAIL  , new String[]{"loh2716@naver.com"});
			intent.putExtra(Intent.EXTRA_SUBJECT, "제목");
			intent.putExtra(Intent.EXTRA_TEXT   , "내용");
			startActivity(Intent.createChooser(intent, "Send mail..."));

		}
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// 하드웨어 뒤로가기 버튼에 따른 이벤트 설정
		case KeyEvent.KEYCODE_BACK:
			Toast.makeText(this, "뒤로가기버튼 눌림", Toast.LENGTH_SHORT).show();
			new AlertDialog.Builder(this).setTitle("프로그램 종료").setMessage("프로그램을 종료 하시겠습니까?")
					.setNegativeButton("예", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							// 프로세스 종료.
							moveTaskToBack(true);
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());

						}
					}).setPositiveButton("아니오", null).show();
			break;
		default:
			break;
		}
		return super.onKeyDown(keyCode, event);
	}
}