package com.example.academyteam.webthread;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import com.example.academyteam.MemberDAO;

import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

public class IdCheckThread extends Thread {
	MemberDAO member;

	public void run() {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		HttpClient client = new DefaultHttpClient();
		String url = "http://htrucci.com:8080/AcademyTeamServer/IdCheck.do";
		HttpPost post = new HttpPost(url);

		List params = new ArrayList();
		params.add(new BasicNameValuePair("id", member.id));
		try {
			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
			post.setEntity(ent);
			HttpResponse responsePOST = client.execute(post);
			HttpEntity resEntity = responsePOST.getEntity();
			if (resEntity != null) {
				/* Log.w("RESPONSE", EntityUtils.toString(resEntity)); */
				XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
				XmlPullParser parser = factory.newPullParser();
				InputStream is = resEntity.getContent();
				parser.setInput(is, "utf-8");
				String tag = "";
				int eventType = parser.getEventType();
				Log.w("RESPONSE", "RESPONSE EXIST!");
				while (eventType != XmlPullParser.END_DOCUMENT) {
					switch (eventType) {
					case XmlPullParser.START_DOCUMENT:
					case XmlPullParser.END_DOCUMENT:
					case XmlPullParser.END_TAG:
						break;
					case XmlPullParser.START_TAG:
						tag = parser.getName();
						break;
					case XmlPullParser.TEXT:
						if (tag.equals("result")) {
							if (parser.getText().equals("SUCCESS")) {
								member.idcheck = "true";
								Log.w("RESPONSE", "idcheck true");
							} else if (parser.getText().equals("FALSE")) {
								member.idcheck = "false";
								Log.w("RESPONSE", "idcheck false");
							}
						}
						break;
					}
					eventType = parser.next();
				}
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
