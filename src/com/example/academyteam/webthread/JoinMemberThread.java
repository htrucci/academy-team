package com.example.academyteam.webthread;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.os.StrictMode;
import android.util.Log;

import com.example.academyteam.MemberDAO;

public class JoinMemberThread extends Thread {
	MemberDAO member;
	public void run() {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		HttpClient client = new DefaultHttpClient();
		String url = "http://htrucci.com:8080/AcademyTeamServer/MemberJoin.do";
		HttpPost post = new HttpPost(url);
		List params = new ArrayList();
		params.add(new BasicNameValuePair("id", member.id));
		params.add(new BasicNameValuePair("password", member.password));
		params.add(new BasicNameValuePair("name", member.name));
		params.add(new BasicNameValuePair("number", member.number));
		try {
			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
			post.setEntity(ent);
			HttpResponse responsePOST = client.execute(post);
			HttpEntity resEntity = responsePOST.getEntity();
			if (resEntity != null) {
				Log.w("RESPONSE", EntityUtils.toString(resEntity));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
