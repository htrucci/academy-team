package com.example.academyteam;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

public class ThirdActivity extends Activity implements OnClickListener {


	Button bc;
	Button citi;
	Button hd;	
	Button lotte;	
	Button oehwan;	
	Button samsung;	
	Button sc;
	Button sinhan;	
	Button sk;
	Button woori;
	Button ibk;
	Button kb;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.third_activity);
		Toast toast=Toast.makeText(getApplicationContext(), "※분실/도난시 이용하는 메뉴입니다.\n해당 카드사를 누르면 연결됩니다.", Toast.LENGTH_LONG);
		toast.show();
		bc=(Button)findViewById(R.id.bc);	
		citi=(Button)findViewById(R.id.citi);	
		hd=(Button)findViewById(R.id.hd);	
		lotte=(Button)findViewById(R.id.lotte);	
		oehwan=(Button)findViewById(R.id.oehwan);	
		samsung=(Button)findViewById(R.id.samsung);	
		sc=(Button)findViewById(R.id.sc);	
		sinhan=(Button)findViewById(R.id.sinhan);	
		sk=(Button)findViewById(R.id.sk);	
		woori=(Button)findViewById(R.id.woori);	
		ibk=(Button)findViewById(R.id.ibk);	
		kb=(Button)findViewById(R.id.kb);	

		bc.setOnClickListener(this);
		citi.setOnClickListener(this);
		hd.setOnClickListener(this);
		lotte.setOnClickListener(this);
		oehwan.setOnClickListener(this);
		samsung.setOnClickListener(this);
		sc.setOnClickListener(this);
		sinhan.setOnClickListener(this);
		sk.setOnClickListener(this);
		woori.setOnClickListener(this);
		ibk.setOnClickListener(this);
		kb.setOnClickListener(this);


	}
	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.bc:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-4000")));
		
			break;

		case R.id.citi:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1566-1000")));
			break;

		case R.id.hd:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1577-6000")));
			break;
		case R.id.lotte:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-8100")));
			break;
		case R.id.oehwan:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-3200")));
			break;
		case R.id.samsung:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-8700")));
			break;
		case R.id.sc:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-1599")));
			break;
		case R.id.sinhan:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-7000")));
			break;
		case R.id.sk:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1599-1155")));
			break;
		case R.id.woori:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-9955")));
			break;
		case R.id.ibk:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1566-2566")));
			break;
		case R.id.kb:
			startActivity(new Intent("android.intent.action.DIAL",Uri.parse("tel:1588-1688")));
			break;


		}

	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// 하드웨어 뒤로가기 버튼에 따른 이벤트 설정
		case KeyEvent.KEYCODE_BACK:
			Toast.makeText(this, "뒤로가기버튼 눌림", Toast.LENGTH_SHORT).show();
			new AlertDialog.Builder(this).setTitle("프로그램 종료").setMessage("프로그램을 종료 하시겠습니까?")
					.setNegativeButton("예", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							// 프로세스 종료.
							moveTaskToBack(true);
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());

						}
					}).setPositiveButton("아니오", null).show();
			break;
		default:
			break;
		}
		return super.onKeyDown(keyCode, event);
	}
}
