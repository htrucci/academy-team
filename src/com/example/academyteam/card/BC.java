package com.example.academyteam.card;

import java.util.Calendar;

import com.example.academyteam.InfoDAO;

import android.util.Log;

public class BC {
	String BC = "01031893477";
	String CREDIT = "기업BC";
	String CANCEL = "취소접수";
	String CHECK = "체크.승인";
	public String type="";
	public String company="";
	public int year=0;
	public int month=0;
	public int day=0;
	public String hour="";
	public String minute="";
	public String pay="0";
	int preposition=0;
	int position=0;
	public String store ="";
	public String category="";
	public void BCcheck(String address, String body){
		if (address.equals(BC)) { // 기업BC 15884000
				// [체크.승인]
				// 1,700원
				// 기업BC(4*3*)황*빈님
				// 10/15 20:02통장잔액332,500원
				// KCP_자동과금
			Log.e("INFO", body.substring(1, 6));
			if (body.substring(1, 6).equals(CHECK)) { // 체크 조건일 때
				type = "체크";
				company = "기업BC";
				Calendar today = Calendar.getInstance(); // 날짜 가져오는 변수, 함수

				year = today.get(Calendar.YEAR); // 현재 날짜(연도)
				month = (today.get(Calendar.MONTH) + 1); // 현재
																			// 날짜(월)
				/*if(month.length()==1) month = "0"+month;*/
				day = today.get(Calendar.DATE); // 현재 날짜(일)
				position = body.indexOf(":"); // 시간 찾기위해 ":" 검색
				hour = body.substring(position - 2, position); // 시 잘라냄
				minute = body.substring(position + 1, position + 3); // 분 잘라냄
				preposition = body.indexOf("]"); // 결제금액 찾기위해 "]" 찾음 (시작위치)
				position = body.indexOf("원"); // "원" (마지막위치)
				pay = body.substring(preposition + 2, position); // 결제금액 잘라냄
				pay = pay.replace(",", ""); // 결제금액중 ","을 공백으로 없앰
				preposition = body.lastIndexOf("\n", body.length() - 2); // 마지막줄이
																			// 가맹점
																			// 정보
																			// (시작위치)
				store = body.substring(preposition + 1, body.length()); // 가맹점
																			// 정보
																			// 잘라냄
				Categorization cate = new Categorization();
				category = cate.categorization(store);
			} else if (body.substring(0, 4).equals(CREDIT)) { // 신용 조건일 때
				// 기업BC(4*3*)황*빈님.10/12 15:34.일시불7,000원.누적금액531,414원.다우랑
				type = "신용";
				company = "기업BC";
				Calendar today = Calendar.getInstance(); // 날짜 가져오는 변수, 함수

				year = today.get(Calendar.YEAR); // 현재 날짜(연도)
				month = (today.get(Calendar.MONTH) + 1); // 현재
																			// 날짜(월)
				day = today.get(Calendar.DATE); // 현재 날짜(일)
				position = body.indexOf(":"); // 시간 찾기위해 ":" 검색
				hour = body.substring(position - 2, position); // 시 잘라냄
				minute = body.substring(position + 1, position + 3); // 분 잘라냄
				preposition = body.indexOf("불"); // 결제금액 찾기위해 "불" 찾음(일시불)
														// (시작위치)
				position = body.indexOf("원"); // "원" (마지막위치)
				pay = body.substring(preposition + 2, position); // 결제금액 잘라냄
				pay = pay.replace(",", ""); // 결제금액중 ","을 공백으로 없앰
				preposition = body.lastIndexOf("."); // 가맹점 정보는 . 다음임
				store = body.substring(preposition + 1, body.length()); // 가맹점
																		// 정보
																		// 잘라냄
				Categorization cate = new Categorization();
				category = cate.categorization(store);
			} else if (body.substring(1, 5).equals(CANCEL)) { // 취소 조건일 때
				// [취소접수]
				// 11,600원
				// 기업BC(4*3*)황*빈님
				// 10/11 00:00
				// 한국철도공사
				type = "취소접수";
				company = "기업BC";
				Calendar today = Calendar.getInstance(); // 날짜 가져오는 변수, 함수

				year = today.get(Calendar.YEAR); // 현재 날짜(연도)
				month = (today.get(Calendar.MONTH) + 1); // 현재
																			// 날짜(월)
				day = today.get(Calendar.DATE); // 현재 날짜(일)
				position = body.indexOf(":"); // 시간 찾기위해 ":" 검색
				hour = body.substring(position - 2, position); // 시 잘라냄
				minute = body.substring(position + 1, position + 3); // 분 잘라냄
				preposition = body.indexOf("]"); // 결제금액 찾기위해 "]" 찾음 (시작위치)
				position = body.indexOf("원"); // "원" (마지막위치)
				pay = body.substring(preposition + 1, position); // 결제금액 잘라냄
				pay = pay.replace(",", ""); // 결제금액중 ","을 공백으로 없앰
				preposition = body.lastIndexOf("\n", body.length() - 2); // 마지막줄이
																			// 가맹점
																			// 정보
																			// (시작위치)
				store = body.substring(preposition + 1, body.length()); // 가맹점
																			// 정보
				Categorization cate = new Categorization();
				category = cate.categorization(store);													// 잘라냄
			}

		} 
	}
}
	
