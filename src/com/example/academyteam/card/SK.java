package com.example.academyteam.card;

import java.util.Calendar;

import com.example.academyteam.InfoDAO;

import android.util.Log;

public class SK {
	String SK = "15991155";
	String CREDITSK = "하나SK";
	String CANCEL = "취소접수";
	String CHECKSK = "체크카드";
	public String type="";
	public String company="";
	public int year=0;
	public int month=0;
	public int day = 0;
	public String hour="";
	public String minute="";
	public String pay="0";
	int preposition=0;
	int position=0;
	public String store ="";
	public String category="";
	public void SKcheck(String address, String body){
		if (address.equals(SK)) { 
			//체크카드(1*1*)김*남님 10/30 11:32/일시불/승인/1,234원/gs
			Log.e("INFO", body.substring(0, 4));
			if (body.substring(0, 4).equals(CHECKSK)) { // 체크 조건일 때
				type = "체크";
				company = "하나SK";
				Calendar today = Calendar.getInstance(); // 날짜 가져오는 변수, 함수

				year = today.get(Calendar.YEAR); // 현재 날짜(연도)
				month = (today.get(Calendar.MONTH) + 1); // 현재
																			// 날짜(월)
				day = today.get(Calendar.DATE); // 현재 날짜(일)
				position = body.indexOf(":"); // 시간 찾기위해 ":" 검색
				hour = body.substring(position - 2, position); // 시 잘라냄
				minute = body.substring(position + 1, position + 3); // 분 잘라냄
				preposition = body.indexOf("승인"); // 결제금액 찾기위해 "]" 찾음 (시작위치)
				position = body.indexOf("원"); // "원" (마지막위치)
				pay = body.substring(preposition + 3, position); // 결제금액 잘라냄
				pay = pay.replace(",", ""); // 결제금액중 ","을 공백으로 없앰
				preposition = body.lastIndexOf("/", body.length() - 2); // 마지막줄이
																			// 가맹점
																			// 정보
																			// (시작위치)
				store = body.substring(preposition + 1, body.length()); // 가맹점
																			// 정보
																			// 잘라냄
				Categorization cate = new Categorization();
				category = cate.categorization(store);
			} else if (body.substring(8, 12).equals(CREDITSK)) { // 신용 조건일 때
				// [web발신]
				//하나SK(1*1*)배*인님
				//10/22 19:17 대우 홈
				//일시불/25,400원/
				//누적1,424,424원
				type = "신용";
				company = "하나SK";
				Calendar today = Calendar.getInstance(); // 날짜 가져오는 변수, 함수

				year = today.get(Calendar.YEAR); // 현재 날짜(연도)
				month = (today.get(Calendar.MONTH) + 1); // 현재
																			// 날짜(월)
				day = today.get(Calendar.DATE); // 현재 날짜(일)
				position = body.indexOf(":"); // 시간 찾기위해 ":" 검색
				hour = body.substring(position - 2, position); // 시 잘라냄
				minute = body.substring(position + 1, position + 3); // 분 잘라냄
				preposition = body.indexOf("불"); // 결제금액 찾기위해 "불" 찾음(일시불)
														// (시작위치)
				position = body.indexOf("원"); // "원" (마지막위치)
				pay = body.substring(preposition + 2, position); // 결제금액 잘라냄
				pay = pay.replace(",", ""); // 결제금액중 ","을 공백으로 없앰
				preposition = body.indexOf(":"); // 시간 찾기위해 ":" 검색
				store = body.substring(preposition + 4, body.indexOf("일")-1); // 가맹점
																		// 정보
																		// 잘라냄
				Categorization cate = new Categorization();
				category = cate.categorization(store);
			} else if (body.substring(1, 5).equals(CANCEL)) { // 취소 조건일 때
				// [취소접수]
				// 11,600원
				// 기업BC(4*3*)황*빈님
				// 10/11 00:00
				// 한국철도공사
				type = "취소접수";
				company = "기업BC";
				Calendar today = Calendar.getInstance(); // 날짜 가져오는 변수, 함수

				year = today.get(Calendar.YEAR); // 현재 날짜(연도)
				month = (today.get(Calendar.MONTH) + 1); // 현재
																			// 날짜(월)
				day = today.get(Calendar.DATE); // 현재 날짜(일)
				position = body.indexOf(":"); // 시간 찾기위해 ":" 검색
				hour = body.substring(position - 2, position); // 시 잘라냄
				minute = body.substring(position + 1, position + 3); // 분 잘라냄
				preposition = body.indexOf("]"); // 결제금액 찾기위해 "]" 찾음 (시작위치)
				position = body.indexOf("원"); // "원" (마지막위치)
				pay = body.substring(preposition + 1, position); // 결제금액 잘라냄
				pay = pay.replace(",", ""); // 결제금액중 ","을 공백으로 없앰
				preposition = body.lastIndexOf("\n", body.length() - 2); // 마지막줄이
																			// 가맹점
																			// 정보
																			// (시작위치)
				store = body.substring(preposition + 1, body.length()); // 가맹점
																			// 정보
				Categorization cate = new Categorization();
				category = cate.categorization(store);													// 잘라냄
			}

		} 
	}
}
	
