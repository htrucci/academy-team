package com.example.academyteam.card;

public class Categorization {
	static String categorization(String store){
		String category;
		String cook[] = {"다래원", "서가앤쿡","피자","치킨","김밥","버거","커피","족발","반점","아웃백","T.G.I","베니건스","VIPS"};
		String living[] = {"편의점","다이소","GS","CU","세븐일레븐","마트","슈퍼","아트","당구장","찜질방","사우나"};
		String fashion[] = {"구찌","프라다","몽블랑","나이키","아디다스","푸마","리복","엄브로","카파","EXR","K-SWISS","올스타","컨버스","폴로","마루","뉴발란스","펜디","D&G","베르사체","디스퀘어드","페레가모","입생로랑","루이비통","끌로에","디올","까르띠에","에르메스"};
		String leisure[] = {"펜션","콘도","리조트","스키장","서커스","래프팅","해수욕장","스노클링","서핑","낚시","페러세일링"};
		String health[]={"병원","의료","용품","보험"};
		String education[]={"학원","문구","문고"};

		for(int i=0; i<cook.length;i++){
			if(store.contains(cook[i])){

				return "음식";
			}	
		}
		for(int i=0; i<living.length;i++){
			if(store.contains(living[i])){
				return "생활문화";
			}
		}
		for(int i=0; i<fashion.length;i++){
			if(store.contains(fashion[i])){

				return "패션";
			}
		}
		for(int i=0; i<leisure.length;i++){
			if(store.contains(leisure[i])){
				return "레저";
			}
		}
		for(int i=0; i<health.length;i++){
			if(store.contains(health[i])){
				return "건강";
			}
		}
		for(int i=0; i<education.length;i++){
			if(store.contains(education[i])){
				return "교육";
			}
		}
		return "미분류";
	}
}
