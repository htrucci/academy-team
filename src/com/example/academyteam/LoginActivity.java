package com.example.academyteam;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.academyteam.webthread.LoginCheckThread;

public class LoginActivity extends Activity implements OnClickListener {
	// FirstActivity fa;
	// 전역변수 선언 (레이아웃에서 요소들 추가한거 쓰기위해서)
	EditText et_id;
	EditText et_pass;
	Button login_btn;
	MemberDAO member;
	Button join_btn;
	ProgressDialog dialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.loginactivity);
		// findViewById로 레이아웃에 추가한 요소들 다 불러와 줍니다.
		et_id = (EditText) findViewById(R.id.et_id);
		et_pass = (EditText) findViewById(R.id.et_pass);
		login_btn = (Button) findViewById(R.id.login_btn);
		join_btn = (Button) findViewById(R.id.join_btn);
		// 버튼요소에 대해서 클릭하면 동작 할 수 있게 setOnClickListener(this); 설정해줘야 해요.
		login_btn.setOnClickListener(this);
		join_btn.setOnClickListener(this);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// 하드웨어 뒤로가기 버튼에 따른 이벤트 설정
		case KeyEvent.KEYCODE_BACK:
			Toast.makeText(this, "뒤로가기버튼 눌림", Toast.LENGTH_SHORT).show();
			new AlertDialog.Builder(this).setTitle("프로그램 종료").setMessage("프로그램을 종료 하시겠습니까?")
					.setPositiveButton("예", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							// 프로세스 종료.
							moveTaskToBack(true);
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());

						}
					}).setNegativeButton("아니오", null).show();
			break;
		default:
			break;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public void onClick(View v) { // 버튼 누를시에 동작하는 부분 v 변수 일단 전달받음
		if (v.getId() == login_btn.getId()) { // v변수가 로그인버튼의 ID랑 같을때 실행되는 부분. 즉,
												// 로그인 버튼일때

			member.id = et_id.getText().toString();
			member.password = et_pass.getText().toString();
			LoginCheckThread thread = new LoginCheckThread();
			thread.start();
			dialog = ProgressDialog.show(this, "", "확인중",true);
			mHandler.sendEmptyMessageDelayed(1001, 3000);

			

		} else if (v.getId() == join_btn.getId()) { // v변수가 로그인버튼의 ID랑 같을때 실행되는
													// 부분. 즉, 회원가입 버튼일때
			Intent intent = new Intent(getApplicationContext(), JoinActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); // 이
																	// 화면(this)에서
																	// JoinActivity로
																	// 화면이동 설정
			startActivity(intent); // 화면 이동 ㄱㄱ
		}
	}
	
	@Override
	protected void onRestart() {
		Log.e("Activity", "LoginAct onRestart");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.e("Activity", "LoginAct onResume");
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		Log.e("Activity", "LoginAct onDestroy");
		super.onDestroy();
	}
	@Override
	protected void onStop() {
		Log.e("Activity", "LoginAct onStop");
		super.onStop();
	}
    Handler mHandler = new Handler() {
    	 
        public void handleMessage(Message msg) {
            if (msg.what == 1001) { // 타임아웃이 발생하면
                dialog.dismiss(); // ProgressDialog를 종료
                if (member.logincheck.equals("true")) {
    				// 다음페이지 이동
    				Intent intent = new Intent(getApplicationContext(), MainActivity.class);
    				intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP); 																				// 설정
    				startActivity(intent); // 화면 이동 ㄱㄱ
    				Toast.makeText(getApplicationContext(), "로그인 성공", 1).show();
    			} else if (member.logincheck.equals("false")) {
    				Toast.makeText(getApplicationContext(), "아이디 비밀번호를 정확히 입력하세요.", 1).show();
    			}
            }
        }
    };
}
