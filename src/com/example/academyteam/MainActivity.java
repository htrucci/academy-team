package com.example.academyteam;


import android.annotation.SuppressLint;
import android.app.ActivityGroup;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff.Mode;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MainActivity extends ActivityGroup {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		 this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);
		tabHost.setup(this.getLocalActivityManager());
		
		
		TabSpec spec1 = tabHost.newTabSpec("Tab1").setContent(R.id.tab1).setIndicator(getString(R.string.tab1));
		spec1.setContent(new Intent(this,FirstActivity.class));
		tabHost.addTab(spec1);
		TabSpec spec2 = tabHost.newTabSpec("Tab2").setContent(R.id.tab2).setIndicator(getString(R.string.tab2));
		spec2.setContent(new Intent(this,SecondActivity.class));
		tabHost.addTab(spec2);

		TabSpec spec3 = tabHost.newTabSpec("Tab3").setContent(R.id.tab3).setIndicator(getString(R.string.tab3));
		spec3.setContent(new Intent(this,ThirdActivity.class));
		tabHost.addTab(spec3);

		TabSpec spec4 = tabHost.newTabSpec("Tab4").setContent(R.id.tab4).setIndicator(getString(R.string.tab4));
		spec4.setContent(new Intent(this,FourthActivity.class));
		tabHost.addTab(spec4);
		
		tabHost.setCurrentTab(0);
		tabHost.getTabWidget().setBackgroundColor(Color.DKGRAY);
		/*tabHost.getTabWidget().getChildAt(0).getLayoutParams().height=25dip;*/
		tabHost.getTabWidget().getChildAt(0).getBackground().setColorFilter(Color.RED, Mode.ADD);
		/*tabHost.getTabWidget().getChildAt(1).getLayoutParams().height = 250;*/
		tabHost.getTabWidget().getChildAt(1).getBackground().setColorFilter(Color.GREEN, Mode.ADD);
		/*tabHost.getTabWidget().getChildAt(2).getLayoutParams().height = 250;*/
		tabHost.getTabWidget().getChildAt(2).getBackground().setColorFilter(Color.CYAN, Mode.ADD);
		/*tabHost.getTabWidget().getChildAt(3).getLayoutParams().height = 250;*/
		tabHost.getTabWidget().getChildAt(3).getBackground().setColorFilter(Color.BLUE, Mode.ADD);
		LinearLayout.LayoutParams tvParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.WRAP_CONTENT);

		LinearLayout rl1 = (LinearLayout) tabHost.getTabWidget().getChildAt(0);
		rl1.setGravity(Gravity.CENTER_VERTICAL);

		TextView tv1 = (TextView) rl1.getChildAt(1);
		tv1.setLayoutParams(tvParams);
		tv1.setTextAppearance(this, android.R.style.TextAppearance_Medium);
		tv1.setPadding(10, 0, 10, 0);
		tv1.setTextSize(13);
		tv1.setTextColor(Color.WHITE);
		tv1.setGravity(Gravity.CENTER);

		LinearLayout rl2 = (LinearLayout) tabHost.getTabWidget().getChildAt(1);
		rl2.setGravity(Gravity.CENTER_VERTICAL);

		TextView tv2 = (TextView) rl2.getChildAt(1);
		tv2.setLayoutParams(tvParams);
		tv2.setTextAppearance(this, android.R.style.TextAppearance_Medium);
		tv2.setTextSize(13);
		tv2.setPadding(10, 0, 10, 0);
		tv2.setTextColor(Color.WHITE);
		tv2.setGravity(Gravity.CENTER);

		LinearLayout rl3 = (LinearLayout) tabHost.getTabWidget().getChildAt(2);
		rl3.setGravity(Gravity.CENTER_VERTICAL);

		TextView tv3 = (TextView) rl3.getChildAt(1);
		tv3.setLayoutParams(tvParams);
		tv3.setTextAppearance(this, android.R.style.TextAppearance_Medium);
		tv3.setPadding(10, 0, 10, 0);
		tv3.setTextSize(13);
		tv3.setTextColor(Color.WHITE);
		tv3.setGravity(Gravity.CENTER);

		LinearLayout rl4 = (LinearLayout) tabHost.getTabWidget().getChildAt(3);
		rl4.setGravity(Gravity.CENTER_VERTICAL);

		TextView tv4 = (TextView) rl4.getChildAt(1);
		tv4.setLayoutParams(tvParams);
		tv4.setTextAppearance(this, android.R.style.TextAppearance_Medium);
		tv4.setPadding(10, 0, 10, 0);
		tv4.setTextSize(13);
		tv4.setTextColor(Color.WHITE);
		tv4.setGravity(Gravity.CENTER);
	}



}
