package com.example.academyteam;

import com.example.academyteam.webthread.ChangePassThread;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AccountManagement2 extends Activity implements OnClickListener {

	EditText am2_pass1;
	EditText am2_pass2;
	EditText am2_pass3;
	Button com_btn;
	MemberDAO member;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.account_management2);
		
		am2_pass2 = (EditText)findViewById(R.id.account2_pw2);
		am2_pass3 = (EditText)findViewById(R.id.account2_pw3);
		com_btn = (Button)findViewById(R.id.account2_btn);
		com_btn.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == com_btn.getId()){
			Thread thread = new ChangePassThread();
			thread.start();
			/*member.changepass = 변경하려는 PASS;*/
			if(member.passcheck.equals("true")){
				// 변경 성공
				member.logincheck = "false";
				Intent intent = new Intent(this	, LoginActivity.class);
				startActivity(intent);
				Toast.makeText(getApplicationContext(),"변경 성공", 1).show();
			}else{
				// 실패..
				Toast.makeText(getApplicationContext(),"변경 실패, 정확히 입력해 주세요.", 1).show();
			}
					
			
		}
		
	}

}
