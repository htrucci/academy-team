package com.example.academyteam;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class DBManager extends SQLiteOpenHelper {

	public DBManager(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		String sql = "CREATE TABLE info (num INTEGER PRIMARY KEY AUTOINCREMENT, address TEXT, message TEXT, type TEXT, company TEXT, year INTEGER, month INTEGER, day INTEGER, hour TEXT, minute TEXT, pay INTEGER, store TEXT, category TEXT, memo TEXT);";
		db.execSQL(sql);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXSITS info");
		// 새로 생성될 수 있도록 onCreate() 메소드를 생성한다.
		onCreate(db);
	}

}
