package com.example.academyteam;

import com.example.academyteam.webthread.IdCheckThread;
import com.example.academyteam.webthread.JoinMemberThread;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class JoinActivity extends Activity implements OnClickListener {
	EditText et_jid;
	EditText et_jpass;
	EditText et_jname;
	EditText et_jpass2;
	EditText et_jphone;
	Button join_btn;
	Button idcheck_btn;
	ProgressDialog dialog;
	MemberDAO member;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.joinactivity);
		et_jid = (EditText)findViewById(R.id.et_jid);
		et_jpass = (EditText)findViewById(R.id.et_jpass);
		et_jname = (EditText)findViewById(R.id.et_jname);
		et_jpass2 = (EditText)findViewById(R.id.et_jpass2);
		et_jphone = (EditText)findViewById(R.id.et_jphone);
		join_btn = (Button)findViewById(R.id.joinsubmit_btn);
		idcheck_btn = (Button)findViewById(R.id.idcheck_btn);
		join_btn.setOnClickListener(this);
		idcheck_btn.setOnClickListener(this);
		
		// 로그인 액티비티 참고해서 가입버튼 누르면 loginactivity로 이동하도록.. 
		// 가입 성공하면 서버에다가 입력한 정보 다 보낼건데요~~
		// 일단 여기서 if문 써서 id 자릿수 맞는지, pass 자릿수, 이름이나 이런거 막 쓰지않았나 검사해주는거 넣어야해요.
		// 방법은 if문 써서 조건문에 et_jid.getText().equals("admin") 식으로 검사해주면 되요.
		// 자릿수는 .length() 로,, 다른 회원가입 사이트 참고해서 이것저것 만들어 주세요~~
		// 로그인, 회원가입 액티비티 레이아웃 디자인도 함께.
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId() == idcheck_btn.getId()){
			member.id = et_jid.getText().toString(); 
			Thread thread = new IdCheckThread();
			thread.start();
			dialog = ProgressDialog.show(this, "", "확인중",true);
			mHandler.sendEmptyMessageDelayed(1001, 3000);
		}
		if(v.getId() == join_btn.getId()){
			if(!(et_jid.getText().length()<=8))
			{
				Toast.makeText(getApplicationContext(),"아이디를 정확히 입력하세요", 1).show();
			}
			else if(!(et_jpass.getText().length()<=10))
			{
				Toast.makeText(getApplicationContext(),"비밀번호를 정확히 입력하세요", 1).show();
			}
			
			else if(!(et_jpass.getText().toString().equals(et_jpass2.getText().toString())))
			{
				Toast.makeText(getApplicationContext(), "비밀번호를 다시 확인하세요", 1).show();
			}else if(member.idcheck.equals("false")){
				Toast.makeText(getApplicationContext(), "ID 중복확인 해주세요", 1).show();
			}
			else if(!(et_jid.getText().length()>=8) && !(et_jpass.getText().length()>=10))
			{
				member.id = et_jid.getText().toString();
				member.password = et_jpass.getText().toString();
				member.name = et_jname.getText().toString();
				member.number = et_jphone.getText().toString();
				Thread jointhread = new JoinMemberThread();
				jointhread.start();
				Intent intent = new Intent(this,LoginActivity.class);
				startActivity(intent);
				Toast.makeText(getApplicationContext(), "회원가입 완료", 1).show();
			}
		}
	}
	  Handler mHandler = new Handler() {
	    	 
	        public void handleMessage(Message msg) {
	            if (msg.what == 1001) { // 타임아웃이 발생하면
	                dialog.dismiss(); // ProgressDialog를 종료
	                if(member.idcheck.equals("true"))
	    				idcheck_btn.setText("가입이 가능한 ID");
	    			else if(member.idcheck.equals("false"))
	    				idcheck_btn.setText("가입이 불가능한 ID");	
	            }
	        }
	    };
}
