package com.example.academyteam;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class SplashActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash);

		Handler handler = new Handler() {

			public void handleMessage(Message msg) {
				Intent intent2 = new Intent(getApplicationContext(), LoginActivity.class)
						.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
				startActivity(intent2);
			}
		};
		handler.sendEmptyMessageDelayed(0, 2000);

	}

	@Override
	protected void onRestart() {
		Log.e("Activity", "Splash onRestart");
		super.onRestart();
	}

	@Override
	protected void onResume() {
		Log.e("Activity", "Splash onResume");
		Intent intent = getIntent();
		String exit = intent.getStringExtra("exit");
		if(exit == null){
			
		}
		else if(exit.equals("goodbye")){
			Log.e("Activity", "goodbye");
			finish();
			android.os.Process.killProcess(android.os.Process.myPid());
		}
		super.onResume();
	}

	@Override
	protected void onDestroy() {
		Log.e("Activity", "Splash onDestroy");
		super.onDestroy();
	}

}
