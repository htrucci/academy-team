package com.example.academyteam;

import com.example.academyteam.month.DateDAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DBManagerHandler {
	private DBManager mDBManager;
	private SQLiteDatabase db;
	Cursor cursor;
	public DBManagerHandler(Context context) {
		this.mDBManager = new DBManager(context, "info.db", null, 1);
	}

	// 닫기
	public void close() {
		db.close();
	}

	// 저장
	public void insert(String address, String message, String type, String company, int year, int month, int day, String hour,
			String minute, int pay, String store, String category, String memo) {
		db = mDBManager.getWritableDatabase();
		ContentValues val = new ContentValues();
		val.put("address", address);
		val.put("message", message);
		val.put("type", type);
		val.put("company", company);
		val.put("year", year);
		val.put("month", month);
		val.put("day", day);
		val.put("hour", hour);
		val.put("minute", minute);
		val.put("pay", pay);
		val.put("store", store);
		val.put("category", category);
		val.put("memo", memo);
		db.insert("info", null, val);	

		
	}

	// 가저오기
	public Cursor select() {
		db = mDBManager.getReadableDatabase();
		Cursor cursor = db.rawQuery("Select * from info", null);
		return cursor;
	}
	public Cursor selectpick(){
		db = mDBManager.getReadableDatabase();
		int startyear = DateDAO.getStartyear();
		int startmonth = DateDAO.getStartmonth();
		int startday = DateDAO.getStartday();
		int endyear = DateDAO.getEndyear();
		int endmonth = DateDAO.getEndmonth();
		int endday = DateDAO.getEndday();
		Cursor cursor = db.rawQuery("Select * from info where year>="+startyear+" and year<="+endyear
				+" and month>="+startmonth+" and month<="+endmonth+" and day>="+startday+" and day<="+endday, null);
		return cursor;
	}
	public Cursor living(){
		db=mDBManager.getReadableDatabase();
		Cursor cursor =db.rawQuery("Select pay from info where category="+"'생활문화'", null);
		return cursor;
	}
	public Cursor cook(){
		db=mDBManager.getReadableDatabase();
		Cursor cursor =db.rawQuery("Select pay from info where category="+"'음식'", null);
		return cursor;
	}
	public Cursor fashion(){
		db=mDBManager.getReadableDatabase();
		Cursor cursor =db.rawQuery("Select pay from info where category="+"'패션'", null);
		return cursor;
	}
	public Cursor health(){
		db=mDBManager.getReadableDatabase();
		Cursor cursor =db.rawQuery("Select pay from info where category="+"'건강'", null);
		return cursor;
	}
	public Cursor education(){
		db=mDBManager.getReadableDatabase();
		Cursor cursor =db.rawQuery("Select pay from info where category="+"'교육'", null);
		return cursor;
	}
	public Cursor leisure(){
		db=mDBManager.getReadableDatabase();
		Cursor cursor =db.rawQuery("Select pay from info where category="+"'레저'", null);
		return cursor;
	}
	public Cursor unclassified(){
		db=mDBManager.getReadableDatabase();
		Cursor cursor =db.rawQuery("Select pay from info where category="+"'미분류'", null);
		return cursor;
	}
	
}
