package com.example.academyteam;

import java.util.Calendar;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import com.example.academyteam.month.DateDAO;
import com.example.academyteam.month.MonthView;

public class FirstActivity extends Activity {
	DateDAO date;
	static ListView lv;
	DBManagerHandler handler;
	ChangeInfoDialog changeinfodialog;
	static ListAdapter adapter;
	public Cursor cursor;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.first_activity);
		MonthView month = (MonthView) findViewById(R.id.monthview);
		month.setOwner((Activity) this);
		date = new DateDAO();
		Calendar today = Calendar.getInstance();

		ListView lv = (ListView) findViewById(R.id.listview);

		String[] from = new String[] { "rowid", "col_1", "col_2", "col_3", "col_4", "col_5", "col_6" };
		int[] to = new int[] { R.id.item1, R.id.item2, R.id.item3, R.id.item4, R.id.item5, R.id.item6, R.id.item7 };
		handler = new DBManagerHandler(getApplicationContext());
		cursor = handler.selectpick();
		if (changeinfodialog == null) // 이름변경 다이얼로그 생성
			changeinfodialog = new ChangeInfoDialog(FirstActivity.this);
		ListMap fillMaps = new ListMap(cursor);
		adapter = new ListAdapter(this, fillMaps.fillMaps, R.layout.grid_item, from, to);
		lv.setAdapter(adapter);


		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				// 리스트 아이템 클릭하면 실행
				changeinfodialog = new ChangeInfoDialog(FirstActivity.this);
				changeinfodialog.setTitle(Integer.toString(position));
				Toast.makeText(getApplicationContext(), (CharSequence) adapter.getItem(position), 3).show();
				changeinfodialog.show();
			}
		});

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// 하드웨어 뒤로가기 버튼에 따른 이벤트 설정
		case KeyEvent.KEYCODE_BACK:
			Toast.makeText(this, "뒤로가기버튼 눌림", Toast.LENGTH_SHORT).show();
			new AlertDialog.Builder(this).setTitle("프로그램 종료").setMessage("프로그램을 종료 하시겠습니까?")
					.setNegativeButton("예", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Intent intent = new Intent(getApplicationContext(), SplashActivity.class);
							intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
							intent.putExtra("exit", "goodbye");
							startActivity(intent);


						}
					}).setPositiveButton("아니오", null).show();
			break;
		default:
			break;
		}
		return super.onKeyDown(keyCode, event);
	}

	public void Test(Cursor cursor) {
		// TODO Auto-generated method stub
		ListMap fillMaps = new ListMap(cursor);
		String[] from = new String[] { "rowid", "col_1", "col_2", "col_3", "col_4", "col_5", "col_6" };
		int[] to = new int[] { R.id.item1, R.id.item2, R.id.item3, R.id.item4, R.id.item5, R.id.item6, R.id.item7 };
		adapter = new ListAdapter(this, fillMaps.fillMaps, R.layout.grid_item, from, to);
		lv = (ListView) findViewById(R.id.listview);
		lv.setAdapter(adapter);
		adapter.notifyDataSetChanged();
	}

}
