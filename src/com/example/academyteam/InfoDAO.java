package com.example.academyteam;

import java.util.Calendar;


import com.example.academyteam.card.*;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

public class InfoDAO {
	public InfoDAO(String id) {
		this.id = id;
	}
	
	DBManagerHandler handler;
	final static String BC = "01031893477";
	final static String NONGHYUP = "010";
	final static String SHINHAN = "15886000";
	final static String CITY = "15661000";
	final static String HYUNDAI = "15776000";
	final static String LOTTE = "15888100";
	final static String OEHWAN = "15883200";
	final static String SAMSUNG = "15888700";
	final static String SC = "15881599";
	final static String SK = "15991155";
	final static String WOORI = "15889955";
	final static String IBK = "15662566";
	final static String KB = "15881688";
	final static String CHECK = "체크.승인";
	final static String CREDIT = "기업BC";
	final static String CANCEL = "취소접수";
	final static String CREDIT1 = "신한카드정상승인";
	final static String CREDITSK = "하나SK";
	final static String CHECKSK = "체크카드";
	
	String address = "";
	String message = "초기값";
	static String id = "abc";

	String pass = "";
	String type = "";
	String company = "";
	String phone = "";
	String name = "";
	int year = 0;
	int month = 0;
	int day = 0;
	String hour = "";
	String minute = "";
	String pay = "0";
	String store = "";
	String category = "";
	String memo = "없음";

	public InfoDAO(String address, String body, Context context) {
		this.message = body;
		this.address = address;
		handler = new DBManagerHandler(context);
		if (address.equals(BC)) { // 기업BC 15884000
			if (body.substring(1, 6).equals(CHECK)) { // 체크 조건일 때
				BC bc = new BC();
				bc.BCcheck(address, body);
				this.type = bc.type;
				this.company = bc.company;
				this.year = bc.year;
				this.month = bc.month;
				this.day = bc.day;
				this.hour = bc.hour;
				this.minute = bc.minute;
				this.pay = bc.pay;
				this.store = bc.store;
				this.category = bc.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				int intpay = Integer.parseInt(pay);
				handler.insert(address, message, type, company, year, month, day, hour, minute, intpay, store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(0, 4).equals(CREDIT)) { // 신용 조건일 때
				BC bc = new BC();
				bc.BCcheck(address, body);
				this.type = bc.type;
				this.company = bc.company;
				this.year = bc.year;
				this.month = bc.month;
				this.day = bc.day;
				this.hour = bc.hour;
				this.minute = bc.minute;
				this.pay = bc.pay;
				this.store = bc.store;
				this.category = bc.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(1, 5).equals(CANCEL)) { // 취소 조건일 때
				BC bc = new BC();
				bc.BCcheck(address, body);
				this.type = bc.type;
				this.company = bc.company;
				this.year = bc.year;
				this.month = bc.month;
				this.day = bc.day;
				this.hour = bc.hour;
				this.minute = bc.minute;
				this.pay = bc.pay;
				this.store = bc.store;
				this.category = bc.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			}
		} else if (address.equals(NONGHYUP)) { // 농협
			// [web발신]
			// [체크.승인]
			// 1,700원
			// NH카드(4*3*)배*식님
			// 10/15 20:02
			// 다리원
			if (body.substring(9, 14).equals(CHECK)) { // 체크 조건일 때
				Nonghyup nh = new Nonghyup();
				nh.NHcheck(address, body);
				this.type = nh.type;
				this.company = nh.company;
				this.year = nh.year;
				this.month = nh.month;
				this.day = nh.day;
				this.hour = nh.hour;
				this.minute = nh.minute;
				this.pay = nh.pay;
				this.store = nh.store;
				this.category = nh.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(0, 4).equals(CREDIT)) { // 신용 조건일 때
				Nonghyup nh = new Nonghyup();
				nh.NHcheck(address, body);
				this.type = nh.type;
				this.company = nh.company;
				this.year = nh.year;
				this.month = nh.month;
				this.day = nh.day;
				this.hour = nh.hour;
				this.minute = nh.minute;
				this.pay = nh.pay;
				this.store = nh.store;
				this.category = nh.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(1, 5).equals(CANCEL)) { // 취소 조건일 때
				Nonghyup nh = new Nonghyup();
				nh.NHcheck(address, body);
				this.type = nh.type;
				this.company = nh.company;
				this.year = nh.year;
				this.month = nh.month;
				this.day = nh.day;
				this.hour = nh.hour;
				this.minute = nh.minute;
				this.pay = nh.pay;
				this.store = nh.store;
				this.category = nh.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			}

		} else if (address.equals(SHINHAN)) { // 신한
			// [web발신]
			// [체크.승인]
			// 1,700원
			// 기업BC(4*3*)황*빈님
			// 10/15 20:02통장잔액332,500원
			// KCP_자동과금
			if (body.substring(1, 6).equals(CHECK)) { // 체크 조건일 때
				Shinhan sh = new Shinhan();
				sh.SHcheck(address, body);
				this.type = sh.type;
				this.company = sh.company;
				this.year = sh.year;
				this.month = sh.month;
				this.day = sh.day;
				this.hour = sh.hour;
				this.minute = sh.minute;
				this.pay = sh.pay;
				this.store = sh.store;
				this.category = sh.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(8, 16).equals(CREDIT1)) { // 신용 조건일 때
				Shinhan sh = new Shinhan();
				sh.SHcheck(address, body);
				this.type = sh.type;
				this.company = sh.company;
				this.year = sh.year;
				this.month = sh.month;
				this.day = sh.day;
				this.hour = sh.hour;
				this.minute = sh.minute;
				this.pay = sh.pay;
				this.store = sh.store;
				this.category = sh.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(1, 5).equals(CANCEL)) { // 취소 조건일 때
				Shinhan sh = new Shinhan();
				sh.SHcheck(address, body);
				this.type = sh.type;
				this.company = sh.company;
				this.year = sh.year;
				this.month = sh.month;
				this.day = sh.day;
				this.hour = sh.hour;
				this.minute = sh.minute;
				this.pay = sh.pay;
				this.store = sh.store;
				this.category = sh.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			}

		}
		else if (address.equals(SK)) { // 하나 SK
			Log.e("SK",body.substring(0,3));
			Log.e("SK CREDIT",body.substring(8,12));
			if (body.substring(0, 4).equals(CHECKSK)) { // 체크 조건일 때
				SK sk = new SK();
				sk.SKcheck(address, body);
				this.type = sk.type;
				this.company = sk.company;
				this.year = sk.year;
				this.month = sk.month;
				this.day = sk.day;
				this.hour = sk.hour;
				this.minute = sk.minute;
				this.pay = sk.pay;
				this.store = sk.store;
				this.category = sk.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(8, 12).equals(CREDITSK)) { // 신용 조건일 때
				SK sk = new SK();
				sk.SKcheck(address, body);
				this.type = sk.type;
				this.company = sk.company;
				this.year = sk.year;
				this.month = sk.month;
				this.day = sk.day;
				this.hour = sk.hour;
				this.minute = sk.minute;
				this.pay = sk.pay;
				this.store = sk.store;
				this.category = sk.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			} else if (body.substring(1, 5).equals(CANCEL)) { // 취소 조건일 때
				Shinhan sh = new Shinhan();
				sh.SHcheck(address, body);
				this.type = sh.type;
				this.company = sh.company;
				this.year = sh.year;
				this.month = sh.month;
				this.day = sh.day;
				this.hour = sh.hour;
				this.minute = sh.minute;
				this.pay = sh.pay;
				this.store = sh.store;
				this.category = sh.category;
				Toast.makeText(context, "가계부에 등록합니다!", Toast.LENGTH_SHORT).show();
				handler.insert(address, message, type, company, year, month, day, hour, minute, Integer.parseInt(pay), store, category, memo);
				handler.close();
				Thread thread = new DbThread(this);
				thread.start();	
			}

		}
	}

}