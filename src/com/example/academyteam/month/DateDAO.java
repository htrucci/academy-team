package com.example.academyteam.month;

public class DateDAO {
	private static String year;
	private static String month;
	private static String day;
	private static int startyear = 1900;
	private static int startmonth= 1;
	public static int getStartyear() {
		return startyear;
	}
	public static void setStartyear(int startyear) {
		DateDAO.startyear = startyear;
	}
	public static int getStartmonth() {
		return startmonth;
	}
	public static void setStartmonth(int startmonth) {
		DateDAO.startmonth = startmonth;
	}
	public static int getStartday() {
		return startday;
	}
	public static void setStartday(int startday) {
		DateDAO.startday = startday;
	}
	public static int getEndyear() {
		return endyear;
	}
	public static void setEndyear(int endyear) {
		DateDAO.endyear = endyear;
	}
	public static int getEndmonth() {
		return endmonth;
	}
	public static void setEndmonth(int endmonth) {
		DateDAO.endmonth = endmonth;
	}
	public static int getEndday() {
		return endday;
	}
	public static void setEndday(int endday) {
		DateDAO.endday = endday;
	}
	private static int startday= 1;
	private static int endyear= 3000;
	private static int endmonth= 12;
	private static int endday= 31;
	public static int sw;

	public DateDAO(){
		
	}
	public String getYear() {
		return year;
	}
	public String getMonth() {
		return month;
	}
	public String getDay() {
		return day;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public void setMonth(String month) {
		this.month = month;
	}
	public void setDay(String day) {
		this.day = day;
	}
}
