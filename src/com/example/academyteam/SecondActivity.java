package com.example.academyteam;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.ViewGroup;
import android.widget.Toast;

import com.handstudio.android.hzgrapherlib.animation.GraphAnimation;
import com.handstudio.android.hzgrapherlib.graphview.BarGraphView;
import com.handstudio.android.hzgrapherlib.graphview.CircleGraphView;
import com.handstudio.android.hzgrapherlib.vo.GraphNameBox;
import com.handstudio.android.hzgrapherlib.vo.bargraph.BarGraph;
import com.handstudio.android.hzgrapherlib.vo.bargraph.BarGraphVO;
import com.handstudio.android.hzgrapherlib.vo.circlegraph.CircleGraph;
import com.handstudio.android.hzgrapherlib.vo.circlegraph.CircleGraphVO;

public class SecondActivity extends Activity {

	private ViewGroup layoutGraphView;
	private ViewGroup layoutGraphView2;
	DBManagerHandler handler;
	int livingtotal = 0;
	int livingtotalcount = 0;
	int cooktotal = 0;
	int cooktotalcount = 0;
	int leisuretotal = 0;
	int leisuretotalcount = 0;
	int healthtotal = 0;
	int healthtotalcount = 0;
	int educationtotal = 0;
	int educationtotalcount = 0;
	int fashiontotal = 0;
	int fashiontotalcount = 0;
	int unclassifiedtotal = 0;
	int unclassifiedtotalcount = 0;



	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.second_activity);

		DB();
		
		layoutGraphView = (ViewGroup) findViewById(R.id.layoutGraphView);
		layoutGraphView2 = (ViewGroup)findViewById(R.id.layoutGraphView2);
		layoutGraphView2.addView (new BarGraphView(this , createBarGraphVO()));
		setCircleGraph();
		
		
	}
	
	void DB(){
		handler = new DBManagerHandler(getApplicationContext());
		Cursor cook = handler.cook();
		Cursor living = handler.living();
		Cursor fashion = handler.fashion();
		Cursor leisure = handler.leisure();
		Cursor health = handler.health();
		Cursor education = handler.education();
		Cursor unclassified = handler.unclassified();
		
		livingtotalcount = living.getCount();
		cooktotalcount = cook.getCount();
		leisuretotalcount = leisure.getCount();
		healthtotalcount = health.getCount();
		educationtotalcount = education.getCount();
		fashiontotalcount = fashion.getCount();
		unclassifiedtotalcount = unclassified.getCount();

		while (living.moveToNext()) {
			livingtotal += living.getInt(0);
		}
		while (cook.moveToNext()) {
			cooktotal += cook.getInt(0);
		}

		while (fashion.moveToNext()) {
			fashiontotal += fashion.getInt(0);
		}

		while (leisure.moveToNext()) {
			leisuretotal += leisure.getInt(0);
		}

		while (health.moveToNext()) {
			healthtotal += health.getInt(0);
		}

		while (education.moveToNext()) {
			educationtotal += education.getInt(0);
		}
		while (unclassified.moveToNext()) {
			unclassifiedtotal += unclassified.getInt(0);
		}
		
		
	}

	private void setCircleGraph() {

		CircleGraphVO vo = makeLineGraphAllSetting();

		layoutGraphView.addView(new CircleGraphView(this, vo));
		
	}

	/**
	 * make line graph using options
	 * 
	 * @return
	 */
	public static String makeMoneyType(double dblMoneyString) 
    { 
            String moneyString = new Double(dblMoneyString).toString(); 

            String format = "#,##0.00"; 
            DecimalFormat df = new DecimalFormat(format); 
            DecimalFormatSymbols dfs = new DecimalFormatSymbols(); 

            dfs.setGroupingSeparator(',');// 구분자를 ,로 
            df.setGroupingSize(3);//3자리 단위마다 구분자처리 한다. 
            df.setDecimalFormatSymbols(dfs); 

            return (df.format(Double.parseDouble(moneyString))).toString(); 
    } 

	private CircleGraphVO makeLineGraphAllSetting() {
		// BASIC LAYOUT SETTING
		// padding
		int paddingBottom = CircleGraphVO.DEFAULT_PADDING;
		int paddingTop = CircleGraphVO.DEFAULT_PADDING;
		int paddingLeft = CircleGraphVO.DEFAULT_PADDING;
		int paddingRight = CircleGraphVO.DEFAULT_PADDING;

		// graph margin
		int marginTop = CircleGraphVO.DEFAULT_MARGIN_TOP;
		int marginRight = CircleGraphVO.DEFAULT_MARGIN_RIGHT;

		// radius setting
		int radius = 130;

		List<CircleGraph> arrGraph = new ArrayList<CircleGraph>();
		DecimalFormat df = new DecimalFormat("###,###");
		if (livingtotalcount > 0) {
			arrGraph.add(new CircleGraph(df.format(livingtotal), Color.parseColor("#FF3333"),
					livingtotalcount, "생활문화"));
		}
		if (cooktotalcount > 0) {
			arrGraph.add(new CircleGraph(df.format(cooktotal), Color.parseColor("#FF9933"),
					cooktotalcount, "음식"));

		}
		if (fashiontotalcount > 0) {
			arrGraph.add(new CircleGraph(df.format(fashiontotal), Color.parseColor("#FFFF33"),
					fashiontotalcount, "패션"));
		}
		if (healthtotalcount > 0) {
			arrGraph.add(new CircleGraph(df.format(healthtotal), Color.parseColor("#CCFF66"),
					healthtotalcount, "건강"));
		}
		if (educationtotalcount > 0) {
			arrGraph.add(new CircleGraph(df.format(educationtotal), Color.parseColor("#99FFCC"),
					educationtotalcount, "교육"));
		}
		if (leisuretotalcount > 0) {
			arrGraph.add(new CircleGraph(df.format(leisuretotal), Color.parseColor("#CC99FF"),
					leisuretotalcount, "레저"));
		}
		if (unclassifiedtotalcount > 0) {
			arrGraph.add(new CircleGraph(df.format(unclassifiedtotal), Color.parseColor("#CC33CC"),
					unclassifiedtotalcount, "미분류"));
		}

		CircleGraphVO vo = new CircleGraphVO(paddingBottom, paddingTop,
				paddingLeft, paddingRight, marginTop, marginRight, radius,
				arrGraph);

		// circle Line
		vo.setLineColor(Color.WHITE);

		// set text setting
		vo.setTextColor(Color.BLACK);
		vo.setTextSize(30);

		// set circle center move X ,Y
		vo.setCenterX(0);
		vo.setCenterY(0);

		// set animation
		vo.setAnimation(new GraphAnimation(GraphAnimation.LINEAR_ANIMATION,
				2000));
		// set graph name box

		vo.setPieChart(true);

		GraphNameBox graphNameBox = new GraphNameBox();

		// nameBox
		graphNameBox.setNameboxMarginTop(25);
		graphNameBox.setNameboxMarginRight(25);
		graphNameBox.setNameboxIconHeight(45);
		graphNameBox.setNameboxIconWidth(40);
		graphNameBox.setNameboxTextSize(30);
		vo.setGraphNameBox(graphNameBox);

		return vo;
	}

	private BarGraphVO createBarGraphVO (){
		BarGraphVO vo = null;
		
		String[] legendArr = {"생활문화","음식","레저","패션","건강","교육","미분류"};

		int maxValue[]={livingtotalcount,cooktotalcount,leisuretotalcount,healthtotalcount,educationtotalcount,fashiontotalcount,unclassifiedtotalcount};
		int maxValuey=0;//BAR그래프 Y축 최대값
		maxValuey=maxValue[0];
		for(int i=0;i<maxValue.length;i++)
		{			
			if(maxValuey<maxValue[i]){
				maxValuey=maxValue[i];
			}
		}
		
		
		float[] graph1 ={cooktotalcount,0,0,0,0,0,0};
		float[] graph2 ={0,livingtotalcount,0,0,0,0,0};
		float[] graph3 ={0,0,fashiontotalcount,0,0,0,0};
		float[] graph4 ={0,0,0,leisuretotalcount,0,0,0};
		float[] graph5 ={0,0,0,0,healthtotalcount,0,0};
		float[] graph6 ={0,0,0,0,0,educationtotalcount,0};
		float[] graph7 ={0,0,0,0,0,0,unclassifiedtotalcount};

		List<BarGraph> arrGraph = new ArrayList<BarGraph>();
		arrGraph.add(new BarGraph("음식", Color.parseColor("#FF3333"), graph1));
		arrGraph.add(new BarGraph("생활문화", Color.parseColor("#FF9933"), graph2));
		arrGraph.add(new BarGraph("패션", Color.parseColor("#FFFF33"), graph3));
		arrGraph.add(new BarGraph("레저", Color.parseColor("#CC99FF"), graph4));
		arrGraph.add(new BarGraph("건강", Color.parseColor("#CCFF66"), graph5));
		arrGraph.add(new BarGraph("교육", Color.parseColor("#99FFCC"), graph6));
		arrGraph.add(new BarGraph("미분류", Color.parseColor("#CC33CC"), graph7));
		
		
		//arrGraph.add(new BarGraph("tizen", Color.RED, graph3));
		
		int paddingTop = BarGraphVO.DEFAULT_PADDING;
		int paddingBottom = BarGraphVO.DEFAULT_PADDING;
		int paddingLeft = BarGraphVO.DEFAULT_PADDING;
		int paddingRight = BarGraphVO.DEFAULT_PADDING;
		int marginTop = BarGraphVO.DEFAULT_MARGIN_TOP;
		int marginRight = BarGraphVO.DEFAULT_MARGIN_RIGHT;
		int minValueX = 0;
		int minValueY = 0;
		int maxValueX = 700;
		int maxValueY = maxValuey;
		Log.e("asdf", Integer.toString(maxValueY));
		int incrementX = 100;
		int incrementY = 1;
		int barWidth = 50;
		
		vo = new BarGraphVO(legendArr, arrGraph, 
				paddingTop, paddingBottom, paddingLeft, paddingRight,
				marginTop, marginRight,
				minValueX, minValueY, maxValueX, maxValueY,
				incrementX, incrementY,
				barWidth,
				-1);
		/*
		vo.setGraphNameBox(new GraphNameBox());*/
		vo.setAnimation(new GraphAnimation(GraphAnimation.LINEAR_ANIMATION, GraphAnimation.DEFAULT_DURATION));
		vo.setAnimationShow(true);
		
		return vo;
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
		// 하드웨어 뒤로가기 버튼에 따른 이벤트 설정
		case KeyEvent.KEYCODE_BACK:
			Toast.makeText(this, "뒤로가기버튼 눌림", Toast.LENGTH_SHORT).show();
			new AlertDialog.Builder(this).setTitle("프로그램 종료").setMessage("프로그램을 종료 하시겠습니까?")
					.setNegativeButton("예", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {

							// 프로세스 종료.
							moveTaskToBack(true);
							finish();
							android.os.Process.killProcess(android.os.Process.myPid());

						}
					}).setPositiveButton("아니오", null).show();
			break;
		default:
			break;
		}
		return super.onKeyDown(keyCode, event);
	}
	
}
