package com.example.academyteam;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import android.os.StrictMode;
import android.util.Log;

public class DbThread extends Thread {
	InfoDAO info;
	public DbThread(InfoDAO info){
		this.info = info;
	}
	public void run() {
		StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork()
				.penaltyLog().build());
		HttpClient client = new DefaultHttpClient();
		String url = "http://htrucci.iptime.org:8080/academyserver/Insert.do";
		HttpPost post = new HttpPost(url);
		List params = new ArrayList();
		params.add(new BasicNameValuePair("id", "123"));
		params.add(new BasicNameValuePair("message", info.message));
		params.add(new BasicNameValuePair("type", info.type));
		params.add(new BasicNameValuePair("company", info.company));
		params.add(new BasicNameValuePair("year", Integer.toString(info.year)));
		params.add(new BasicNameValuePair("month", Integer.toString(info.month)));
		params.add(new BasicNameValuePair("day", Integer.toString(info.day)));
		params.add(new BasicNameValuePair("hour", info.hour));
		params.add(new BasicNameValuePair("minute", info.minute));
		params.add(new BasicNameValuePair("pay", info.pay));
		params.add(new BasicNameValuePair("store", info.store));
		params.add(new BasicNameValuePair("category", info.category));
		params.add(new BasicNameValuePair("memo", info.memo));
		try {
			UrlEncodedFormEntity ent = new UrlEncodedFormEntity(params, HTTP.UTF_8);
			post.setEntity(ent);
			HttpResponse responsePOST = client.execute(post);
			HttpEntity resEntity = responsePOST.getEntity();
			if (resEntity != null) {
				Log.w("RESPONSE", EntityUtils.toString(resEntity));
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
